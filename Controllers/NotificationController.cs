﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class NotificationController : Controller
    {
        public async Task<JsonResult> Create(CancellationToken cancellationToken = default)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new System.Uri("https://onesignal.com");
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Basic YmZhMjdhMTQtYzFjMC00ZmY4LWIwN2UtNGZkZGUzZDk5ZmFl");

                var model = new OneSignalCreateNotificationModel()
                {
                    AppId = "adc203d8-957b-44b1-9831-bcb5a6d22f02",
                    IncludedSegments = new string[] { "Subscribed Users" },
                    contents = new Dictionary<string, string>()
                    {
                        { "en", "Hello my name is Cyrus - API" }
                    }
                };

                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync("/api/v1/notifications", content, cancellationToken).ConfigureAwait(false);
                if (response != null && response.IsSuccessStatusCode)
                {
                    var raw = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    if (!string.IsNullOrEmpty(raw))
                    {
                        var result = JsonConvert.DeserializeObject<OneSignalCreateNotificationResultModel>(raw);
                        if (!string.IsNullOrEmpty(result.Id))
                        {
                            // success
                            return Json(true, JsonRequestBehavior.DenyGet);
                        }
                    }
                }
            }

            return Json(false, JsonRequestBehavior.DenyGet);
        }
    }

    public class OneSignalCreateNotificationModel
    {
        [JsonProperty("app_id")]
        public string AppId { get; set; }
        [JsonProperty("included_segments")]
        public string[] IncludedSegments { get; set; }
        [JsonProperty("data")]
        public object Data { get; set; }
        [JsonProperty("contents")]
        public object contents { get; set; }
    }

    public class OneSignalCreateNotificationResultModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("recipients")]
        public int RecipientsCount { get; set; }
        [JsonProperty("external_id")]
        public object external_id { get; set; }
    }

}
